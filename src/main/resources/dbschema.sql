DROP DATABASE IF EXISTS `users`;
CREATE DATABASE  IF NOT EXISTS `users`;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `users`.`users`;
CREATE TABLE `users`.`users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `dni_number` varchar(45) NOT NULL,
   `phone` varchar(45) NOT NULL,
  `birthdate` date NOT NULL,
  `mail` varchar(45) NOT NULL,
  `password` varchar(70) NOT NULL,
  `id_role` bigint NOT NULL,
  PRIMARY KEY (`id`)
);


--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `users`.`role`;
CREATE TABLE `users`.`role` (
  `id` bigint(20) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `users`.`users`
ADD CONSTRAINT `fk_users_role`
  FOREIGN KEY (`id_role`)
  REFERENCES `users`.`role` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


