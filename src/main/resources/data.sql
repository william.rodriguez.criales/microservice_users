INSERT INTO `users`.`role` (`id`, `description`, `name`) VALUES ('1', 'ROLE_ADMIN', 'ROLE_ADMIN');
INSERT INTO `users`.`role` (`id`, `description`, `name`) VALUES ('2', 'ROLE_EMPLOYEE', 'ROLE_EMPLOYEE');
INSERT INTO `users`.`role` (`id`, `description`, `name`) VALUES ('3', 'ROLE_CLIENT', 'ROLE_CLIENT');
INSERT INTO `users`.`role` (`id`, `description`, `name`) VALUES ('4', 'ROLE_OWNER', 'ROLE_OWNER');
INSERT INTO `users`.`users` (`id`, `name`, `surname`, `dni_number`, `phone`, `birthdate`, `mail`, `password`, `id_role`) VALUES ('1', 'william', 'rodriguez', '555555', '+573023826662', '1992-05-25', 'admin1@outlook.com', '$2a$10$fwjdFHN2EemHKLy51D3yZOlzw07K1WTgHRyV/PoxuuYZhqy0Mk5pq', '1');
