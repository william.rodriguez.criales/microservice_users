package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IUserHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor

public class UserRestController {
    private final IUserHandler personHandler;
    private final IUserHandler roleHandler;
    @SecurityRequirement(name = "jwt")
    @Operation(summary = "Add a new Owner",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Owner created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "user already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})

    @PostMapping
    public ResponseEntity<UserResponseDto> saveUser(@RequestBody UserRequestDto userRequestDto) {
        UserResponseDto userResponseDto = personHandler.saveUser(userRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(userResponseDto);


    }
    @SecurityRequirement(name = "jwt")
    @Operation(summary = "get user by id",
            responses = {
                    @ApiResponse(responseCode = "200", description = "user found",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = UserResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "user no found",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Map.class)))})

    @GetMapping("/{id}")
    public ResponseEntity<UserResponseDto> getById(@PathVariable Long id) {
        UserResponseDto userResponseDto = personHandler.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(userResponseDto);


    }
    @SecurityRequirement(name = "jwt")
    @Operation(summary = "Add account employee",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Account employee created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "user already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("/employee")
    public ResponseEntity<UserResponseDto> saveAccountEmployee(@RequestBody UserRequestDto userRequestDto) {
        UserResponseDto accountEmployeeResponseDto = personHandler.saveAccountEmployee(userRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(accountEmployeeResponseDto);
    }

    @Operation(summary = "Add account client",
            responses = {
                    @ApiResponse(responseCode = "201", description = "client employee created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "user already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("/client")
    public ResponseEntity<UserResponseDto> saveAccountClient(@RequestBody UserRequestDto userRequestDto) {
        UserResponseDto accountClientResponseDto = personHandler.saveAccountClient(userRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(accountClientResponseDto);
    }
}
