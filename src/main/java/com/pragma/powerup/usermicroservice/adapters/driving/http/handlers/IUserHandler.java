package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.LoginRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.JwtResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.domain.model.User;

public interface IUserHandler {
    UserResponseDto saveUser(UserRequestDto userRequestDto);

    //UserResponseDto saveUser(UserRequestDto userRequestDto, Long idRole);
    UserResponseDto getById(Long id);


    JwtResponseDto getUserAuth(LoginRequestDto loginRequestDto);

    UserResponseDto saveAccountEmployee(UserRequestDto userRequestDto);

    UserResponseDto saveAccountClient(UserRequestDto userRequestDto);


}
