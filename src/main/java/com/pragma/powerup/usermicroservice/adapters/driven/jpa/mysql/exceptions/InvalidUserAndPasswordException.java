package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions;

public class InvalidUserAndPasswordException extends RuntimeException {

    public InvalidUserAndPasswordException() {
        super();
    }
}
