package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions;

public class RoleNotFoundException extends RuntimeException {

    public RoleNotFoundException() {
            super();
    }


    public RoleNotFoundException(String message) {
        super(message);
    }
}
