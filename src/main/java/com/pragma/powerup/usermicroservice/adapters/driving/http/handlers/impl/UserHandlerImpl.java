package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.LoginRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.JwtResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IUserHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IUserRequestMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IUserResponseMapper;
import com.pragma.powerup.usermicroservice.configuration.Constants;

import com.pragma.powerup.usermicroservice.configuration.security.jwt.JwtProvider;
import com.pragma.powerup.usermicroservice.domain.api.IUserServicePort;
import com.pragma.powerup.usermicroservice.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.pragma.powerup.usermicroservice.configuration.Constants.EMPLOYEE_ROLE_ID;


@Service
@RequiredArgsConstructor
public class UserHandlerImpl implements IUserHandler {
    private final IUserServicePort personServicePort;
    private final IUserRequestMapper personRequestMapper;
    private final IUserResponseMapper personResponseMapper;
    private final JwtProvider jwtProvider;

    @Override
    public UserResponseDto saveUser(UserRequestDto userRequestDto) {
        User userToSave = personRequestMapper.toUser(userRequestDto);
        userToSave.setIdRole(Constants.OWNER_ROLE_ID);
        User user = personServicePort.saveUser(userToSave);
        return personResponseMapper.toUserResponseDto(user);

    }

    @Override
    public UserResponseDto getById(Long id) {
        User user = personServicePort.getById(id);
        return personResponseMapper.toUserResponseDto(user);
    }


    @Override
    public JwtResponseDto getUserAuth(LoginRequestDto loginRequestDto) {
        User userByEmailAndPassword = personServicePort.getUserByEmailAndPassword(loginRequestDto.getUserDni(),
                loginRequestDto.getPassword());

        /*
        List<GrantedAuthority> authorities = List.of(new SimpleGrantedAuthority(String.valueOf(userByEmailAndPassword
                .getIdRole())));
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userByEmailAndPassword.getMail(),
                        userByEmailAndPassword.getPassword(),
                        authorities)
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
         */

        String jwt = jwtProvider.generateToken(userByEmailAndPassword);
        return new JwtResponseDto(jwt);
    }

    @Override
    public UserResponseDto saveAccountEmployee(UserRequestDto userRequestDto) {
        User userToSave = personRequestMapper.toUser(userRequestDto);
        userToSave.setIdRole(EMPLOYEE_ROLE_ID);
        User user = personServicePort.saveUser(userToSave);
        return personResponseMapper.toUserResponseDto(user);
    }

    @Override
    public  UserResponseDto saveAccountClient(UserRequestDto userRequestDto){
        User userToSave = personRequestMapper.toUser(userRequestDto);
        userToSave.setIdRole(Constants.CLIENT_ROLE_ID);
        User user = personServicePort.saveUser(userToSave);
        return personResponseMapper.toUserResponseDto(user);
    }
/*
    private Boolean validateRole(Long idRole){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        boolean isAuthorized = false;
        for (GrantedAuthority grantedAuthority:
             authorities)  {
            if (grantedAuthority.getAuthority().equals(idRole.toString())){
                isAuthorized=true;
            }

        }
        return isAuthorized;
    }

 */

}
