package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.InvalidUserAndPasswordException;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.MailAlreadyExistsException;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.RoleNotFoundException;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.UserNotFoundException;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IRoleRepository;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IUserRepository;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IUserEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.InvalidUserAndPasswordException;
import com.pragma.powerup.usermicroservice.domain.model.User;
import com.pragma.powerup.usermicroservice.domain.spi.IUserPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

@RequiredArgsConstructor
public class UserMysqlAdapter implements IUserPersistencePort {
    private final IUserRepository userRepository;
    private final IUserEntityMapper userEntityMapper;
    private final PasswordEncoder passwordEncoder;
    private final IRoleRepository iRoleRepository;

    @Override
    public User saveUser(User user) {

        if (!iRoleRepository.findById(user.getIdRole()).isPresent()) {
            throw new RoleNotFoundException();
        }

        if (userRepository.existsByMail(user.getMail())) {
            throw new MailAlreadyExistsException();
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        UserEntity userEntity = userRepository.saveAndFlush(userEntityMapper.toEntity(user));
        return userEntityMapper.toUser(userEntity);


    }

    @Override
    public User getById(Long id) {

        Optional<UserEntity> userEntity = userRepository.findById(id);

        if (!userEntity.isPresent()) {
            throw new UserNotFoundException();
        }

        return userEntityMapper.toUser(userEntity.get());
    }

    @Override
    public User getUserByEmailAndPassword(String mail, String password) {




        Optional<UserEntity> byEmailAndPassword = userRepository.findByMail(mail);

        if (!byEmailAndPassword.isPresent()) {
            throw new InvalidUserAndPasswordException();
        }

        UserEntity userEntity = byEmailAndPassword.get();
        if ( !passwordEncoder.matches(password,userEntity.getPassword() )){
            throw new InvalidUserAndPasswordException();
        }
        return userEntityMapper.toUser(userEntity);

    }

    @Override
    public User saveAccountEmployee(User user) {

        if (!iRoleRepository.findById(user.getIdRole()).isPresent()) {
            throw new RoleNotFoundException();
        }

        if (userRepository.existsByMail(user.getMail())) {
            throw new MailAlreadyExistsException();
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        UserEntity userEntity = userRepository.saveAndFlush(userEntityMapper.toEntity(user));
        return userEntityMapper.toUser(userEntity);


    }

    @Override
    public User saveAccountClient(User user) {

        if (!iRoleRepository.findById(user.getIdRole()).isPresent()) {
            throw new RoleNotFoundException();
        }

        if (userRepository.existsByMail(user.getMail())) {
            throw new MailAlreadyExistsException();
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        UserEntity userEntity = userRepository.saveAndFlush(userEntityMapper.toEntity(user));
        return userEntityMapper.toUser(userEntity);

    }


}
