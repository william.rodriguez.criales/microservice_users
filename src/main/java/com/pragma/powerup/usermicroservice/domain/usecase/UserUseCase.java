package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.domain.exceptions.ValidationException;
import com.pragma.powerup.usermicroservice.domain.model.User;
import com.pragma.powerup.usermicroservice.domain.spi.IUserPersistencePort;
import com.pragma.powerup.usermicroservice.domain.api.IUserServicePort;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.Period;

import static com.pragma.powerup.usermicroservice.configuration.Constants.EMPLOYEE_ROLE_ID;

public class UserUseCase implements IUserServicePort {
    private final IUserPersistencePort userPersistencePort;

    public UserUseCase(IUserPersistencePort userPersistencePort) {
        this.userPersistencePort = userPersistencePort;
    }

    @Override
    public User saveUser(User user) {

        if (isEmpty(user.getName())) {
            throw new ValidationException("empty name");
        }

        if (isEmpty(user.getSurname())) {
            throw new ValidationException("empty surname");
        }

        if (isEmpty(user.getDniNumber())) {
            throw new ValidationException("empty Dni number");
        } else if (!isDniNumberValid(user.getDniNumber())) {
            throw new ValidationException(("invalid Dni Number"));
        }

        if (isEmpty(user.getPhone())) {
            throw new ValidationException("empty phone");
        } else if (!isPhoneValid(user.getPhone())) {
            throw new ValidationException("invalid phone");
        }

        if (isDateEmpty(user.getBirthdate())) {
            throw new ValidationException("empty birthdate");
        } else if (!isAgeGreaterThan18(user.getBirthdate())) {
            throw new ValidationException(("the person is lower than 18 years old"));
        }


        if (isEmpty(user.getMail())) {
            throw new ValidationException("empty email");
        } else if (!isEmailValid(user.getMail())) {
            throw new ValidationException("invalid email");
        }



        if (isEmpty(user.getPassword())) {
            throw new ValidationException("empty password");
        }

        return userPersistencePort.saveUser(user);
    }

    @Override
    public User getById(Long id) {
        return userPersistencePort.getById(id);
    }


    @Override
    public User getUserByEmailAndPassword(String mail, String password) {
        //TODO: validations
        return userPersistencePort.getUserByEmailAndPassword(mail, password);
    }


    //validacion email
    private boolean isEmailValid(String email) {
        return email.matches("^[^@]+@[^@]+\\.[a-zA-Z]{2,}$");
    }


    //validacion phone: numeros menores que 13 digitos y mayores a 7 digitos, debe permiter el +
    private boolean isPhoneValid(String phone) {
        return phone.matches("^[+]?(\\d){7,12}$");

    }

    private boolean isDniNumberValid(String dniNumber) {
        return dniNumber.matches("^(\\d)*$");
    }

    private boolean isAgeGreaterThan18(LocalDate birthDate) {
        LocalDate now = LocalDate.now();
        return Period.between(birthDate, now).getYears()>18;
    }

    private boolean isEmpty(String field) {
        //return field.matches("^[''' ']*$") || field==null ; opcion 1
        return StringUtils.isEmpty(field);
    }

    private boolean isDateEmpty(LocalDate field) {
        return ObjectUtils.isEmpty(field);
    }

    private boolean isEmptyNumber(Object field) {
        //return field.matches("^[''' ']*$") || field==null ; opcion 1
        return org.springframework.util.ObjectUtils.isEmpty(field);
    }





}
