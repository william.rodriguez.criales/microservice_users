package com.pragma.powerup.usermicroservice.domain.api;

import com.pragma.powerup.usermicroservice.domain.model.User;

public interface IUserServicePort {
    User saveUser(User user);
    User getById(Long id);


    User getUserByEmailAndPassword (String mail, String password);


}
