package com.pragma.powerup.usermicroservice.domain.spi;

import com.pragma.powerup.usermicroservice.domain.model.User;

public interface IUserPersistencePort {
    User saveUser(User user);

    User getById(Long id);


    User getUserByEmailAndPassword(String mail, String password);

    User saveAccountEmployee(User user);

    User saveAccountClient(User user);

}


