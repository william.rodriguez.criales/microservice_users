package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.domain.exceptions.ValidationException;
import com.pragma.powerup.usermicroservice.domain.model.User;
import com.pragma.powerup.usermicroservice.domain.spi.IUserPersistencePort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserUseCaseTest {

    @Mock
    IUserPersistencePort iUserPersistencePort;
    @InjectMocks
    UserUseCase userUseCase;


    @ParameterizedTest
    @MethodSource("invalidRequest")
    void saveUser(User user, String message) {
        //arrange

        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> userUseCase.saveUser(user));
        //assert
        assertEquals(message, validationException.getMessage());
        verify(iUserPersistencePort, never()).saveUser(user);
    }


    static Stream<Arguments> invalidRequest() {
        Long age = 19L;
        return Stream.of(Arguments.of(new User(1L, "", "", "", "", LocalDate.now(), "", "", null),
                        "empty name"),
                Arguments.of(new User(1L, "name", "", "", "", LocalDate.now(), "", "", null),
                        "empty surname"),
                Arguments.of(new User(1L, "name", "surname", "11111", "+573156204432", LocalDate.now().minusYears(age), "", "1111", null),
                        "empty email"),
                Arguments.of(new User(1L, "name", "surname", "11111", "1111111", LocalDate.now().minusYears(age), "email.co", "3rdfe",  null),
                        "invalid email"),
                Arguments.of(new User(1L, "name", "surname", "43567", "", LocalDate.now().minusYears(age), "email@gmail.com", "yyyy", null),
                        "empty phone"),
                Arguments.of(new User(1L, "name", "surname", "777777", "111", LocalDate.now().minusYears(age), "email@gmail.com", "9999999", null),
                        "invalid phone"),
                Arguments.of(new User(1L, "name", "surname", "", "+5711111111", LocalDate.now().minusYears(age), "email@gmail.com", "kkkkkk", null),
                        "empty Dni number"),
                Arguments.of(new User(1L, "name", "surname", "8888888888", "+5711111111", LocalDate.now().minusYears(age), "email@gmail.com", "jjjjjj", 4L),
                        "invalid Dni Number"),
                Arguments.of(new User(1L, "name", "surname", "444444", "+5711111111", LocalDate.now().minusYears(age), "email@gmail.com", "", null),
                        "empty password"),
                Arguments.of(new User(1L, "name", "surname", "777777", "+5711111111", LocalDate.now(), "email@gmail.com", "email", 4L),
                        "the person is lower than 18 years old")


        );

    }

    @Test
    void saveUserSuccessfully() {
        //arrange
        User user = new User(1L, "name", "surname", "1111111", "+571111111", LocalDate.of(1992, 3, 3), "string@gmail.com", "111111", 1L) ;
        //when
        when(iUserPersistencePort.saveUser(any(User.class))).thenReturn(user);
        //act
        userUseCase.saveUser(user);
        //assert
        verify(iUserPersistencePort, times(1)).saveUser(user);
    }


}
